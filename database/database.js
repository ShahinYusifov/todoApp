const mysql = require("mysql");
const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "todo",
});

connection.connect(function (err) {
    if (err) {
        return console.error("error: " + err.message);
    }
});

module.exports = connection;
