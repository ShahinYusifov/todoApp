const express = require("express");
const router = express.Router();
const connection = require('../database/database')

/**
 * @swagger
 * /tasks/:id:
 *  put:
 *    description: Update tasks
 *    tags:
 *    - tasks
 *    parameters:
 *    - name: title
 *      description: title of the task
 *      in: formData
 *      required: true
 *      type: string 
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
 router.put("/tasks/:id", (req, res) => {
  const id = req.params.id;
  const data = req.body;
  connection.query("UPDATE tasks SET title = ?,description = ? WHERE task_id = ?",
    [data.title,data.description,id],    
    (err, result) => {
      if (err) {
        res.json(err);
      } else {
        res.status(200).json(result);
      }
    }
  );
});


/**
 * @swagger
 * /tasks/:id:
 *   delete:
 *    description: Delete tasks
 *    tags:
 *    - tasks
 *    parameters:
 *    - name: title
 *      description: title of the task
 *      in: formData
 *      required: true
 *      type: string 
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
 router.delete("/tasks/:id", (req, res) => {
  const id = req.params.id;
  connection.query(`DELETE FROM tasks WHERE task_id=${id}`, [],  (err, result) => {
    if (err) {
      res.json(err);
    } else {
      res.status(200).json(result);
    }
  });
});

/**
 * @swagger
 * /tasks:
 *  post:
 *    description: Create tasks
 *    tags:
 *    - tasks
 *    parameters:
 *    - name: id
 *      description: id of the task
 *      in: formData
 *      required: true
 *      type: string 
 *    responses:
 *      200:
 *        Description: Success
 * 
 */

 router.post("/tasks", (req, res) => {
  connection.query(
    `INSERT INTO tasks(task_id,title,description,is_done,user_id) VALUES(?,?,?,?,?)`,
    [
      req.body.task_id,
      req.body.title,
      req.body.description,
      req.body.is_done,
      req.body.user_id,
    ],
    (err, result) => {
      if (err) {
        res.json(err);
      } else {
        res.status(200).json(result);
      }
    }
  );
});

/**
 * @swagger
 * /tasks-noncompleted:
 *  get:
 *    description: Get noncompleted tasks
 *    tags:
 *    - tasks
 *    responses:
 *      200:
 *        Description: Success
 * 
 */

 router.get("/tasks/none-completed", (req, res) => {
  connection.query("SELECT * FROM tasks WHERE is_done = 0", [], (err, result) => {
    if (err) {
      res.json(err);
    } else {
      res.status(200).json(result);
    }
  });
});


/**
 * @swagger
 * /tasks-completed:
 *  get:
 *    description: Get tasks completed
 *    tags:
 *    - tasks
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
 router.get("/tasks/completed", (req, res) => {
  connection.query("SELECT * FROM tasks WHERE is_done = 1",  [], (err, result) => {
    if (err) {
      res.json(err);
    } else {
      res.status(200).json(result);
    }
  });
});


/**
 * @swagger
 * /tasks:
 *  get:
 *    description: Get All Tasks
 *    tags:
 *    - tasks
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
 router.get("/tasks", (req, res) => {
  connection.query("SELECT * FROM tasks", (err, result) => {
    if (err) {
      res.json(err);
    } else {
      res.status(200).json(result);
    }
  });
});

module.exports = router
