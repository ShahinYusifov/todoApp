const express = require("express");
const router = express.Router();
const connection = require('../database/database');


/**
 * @swagger
 * /users/:id:
 *  put:
 *    description: Update users
 *    tags:
 *    - users
 *    parameters:
 *    - name: title
 *      description: title of the user
 *      in: formData
 *      required: true
 *      type: string 
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
 router.put("/users/:id", (req, res) => {
  const id = req.params.id;
  const data = req.body;
  connection.query("UPDATE users SET name = ?,is_blocked = ? WHERE user_id = ?",
    [data.name,data.is_blocked,id],    
    (err, result) => {
      if (err) {
        res.json(err);
      } else {
        res.status(200).json(result);
      }
    }
  );
});


/**
 * @swagger
 * /users/:id:
 *   delete:
 *    description: Delete tasks
 *    tags:
 *    - users
 *    parameters:
 *    - name: title
 *      description: title of the task
 *      in: formData
 *      required: true
 *      type: string 
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
 router.delete("/users/:id", (req, res) => {
  const id = req.params.id;
  connection.query(`UPDATE users SET is_deleted = 1 WHERE user_id=${id}`, (err, result) => {
    if (err) {
      res.json(err);
    } else {
      res.status(200).json(result);
    }
  });
});

/**
 * @swagger
 * /users:
 *  post:
 *    description: Create user
 *    tags:
 *    - users
 *    parameters:
 *    - name: id
 *      description: id of the user
 *      in: formData
 *      required: true
 *      type: string 
 *    responses:
 *      200:
 *        Description: Success
 * 
 */

 router.post("/users", (req, res) => {
  connection.query(
    `INSERT INTO users(user_id,name,is_blocked) VALUES(?,?,?)`,
    [
      req.body.user_id,
      req.body.name,
      req.body.is_blocked,
    ],
    (err, result) => {
      if (err) {
        res.json(err);
      } else {
        res.status(200).json(result);
      }
    }
  );
});

/**
 * @swagger
 * /users-nonblocked:
 *  get:
 *    description: Get non-blocked users
 *    tags:
 *    - users
 *    responses:
 *      200:
 *        Description: Success
 * 
 */

 router.get("/users-nonblocked", (req, res) => {
  connection.query("SELECT * FROM users WHERE is_blocked = 0", (err, result) => {
    if (err) {
      res.json(err);
    } else {
      res.status(200).json(result);
    }
  });
});


/**
 * @swagger
 * /users-blocked:
 *  get:
 *    description: Get blocked users
 *    tags:
 *    - users
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
 router.get("/users-blocked", (req, res) => {
  connection.query("SELECT * FROM users WHERE is_blocked = 1", (err, result) => {
    if (err) {
      res.json(err);
    } else {
      res.status(200).json(result);
    }
  });
});


/**
 * @swagger
 * /users:
 *  get:
 *    description: Get All Users
 *    tags:
 *    - users
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
 router.get("/users", (req, res) => {
  connection.query("SELECT * FROM users WHERE is_deleted=0", (err, result) => {
    if (err) {
      res.json(err);
    } else {
      res.status(200).json(result);
    }
  });
});

module.exports = router
