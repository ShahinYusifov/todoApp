const express = require("express");
const router = express.Router();
const connection = require('../database/database');

/**
 * @swagger
 * /login:
 *  post:
 *    description: Login User's Page
 *    tags:
 *    - Login and Register
 *    parameters:
 *    - name: username
 *      description: username 
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: password
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
router.post('/login', (req, res) => {
  const username = req.body.username
  const password = req.body.password

  if(username && password) {
    connection.query("SELECT * FROM registered_users WHERE username = ? and password = ?",[username,password],
    (err,result)=>{
      if(result.length > 0) {
        res.json("Success")
      } else {
        res.json("Incorrect username and password")
      }
    })
  } else {
    res.json("Please enter your username and password")
  }
})

/**
 * @swagger
 * /register:
 *  post:
 *    description: Register new user
 *    tags:
 *    - Login and Register
 *    parameters:
 *    - name: username
 *      description: id of the task
 *      in: formData
 *      required: true
 *      type: string
 *    - name: password
 *      description: id of the task
 *      in: formData
 *      required: true
 *      type: string
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
router.post("/register", (req, res) => {
  const id = req.params.register_id;
  const username = req.body.username;
  const password = req.body.password;

  connection.query(
    "SELECT register_id FROM registered_users WHERE username = ?",
    [username],
    (err, result) => {
      if (result.length > 0) {
        res.json("Username already exists");
      } else {
        connection.query(
          "INSERT INTO registered_users VALUES(?, ?, ?)",
          [id, username, password],
          (err, result) => {
            if (err) {
              res.json(err);
            } else {
              res.status(200).json(result);
            }
          }
        );
      }
    }
  );
});

/**
 * @swagger
 * /login:
 *  get:
 *    description: Login Page
 *    tags:
 *    - Login and Register
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
router.get('/login', (req,res)=>{
  res.json("Login page")
})

/**
 * @swagger
 * /register:
 *  get:
 *    description: Register Page
 *    tags:
 *    - Login and Register
 *    responses:
 *      200:
 *        Description: Success
 * 
 */
router.get("/register", (req, res) => {
  res.json("Hello My Register Page");
});

module.exports = router;
