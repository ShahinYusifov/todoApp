const express = require("express");
const app = express();
const taskService = require('./routes/task_service')
const users = require('./routes/users');
const register = require('./routes/register');
const swaggerDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const cors = require('cors');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors({
  origin: 'http://localhost:4200'
}));

const swaggerOptions = {
  swaggerDefinition:{
    info:{
      title:'Library API',
      version:'1.0.0'
    }
  },
  apis:['./routes/*.js'],
}

const swaggerDocs = swaggerDoc(swaggerOptions);

app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocs))
app.use(users);
app.use(taskService);
app.use(register);

app.listen(3000, () => {
  console.log("Server is running port 3000");
});
